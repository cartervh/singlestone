# API Sample

API Development Sample

This code was used as a challenge to myself to create a small API which could read and update a simple SQL database. The project uses Maven and Java 10 to instantiate a database of "Contacts" and some appropriate child records, upon which modifications and additions can be made.

## Setup

Requires a tomcat server, Java 10, and Maven to run.
In the tomcat-users.xml, ensure the following lines exist:
```
<role rolename="manager-script"/>
<user username="maven" password="maven" roles="manager-script"/>
```

Run the following Maven command while the server is running:
```
clean install tomcat7:redeploy
```
This will run all the tests

The database is an in-memory H2 database which is configured to initialize on server startup.

## Usage

The database can be accessed through an API, found at localhost:8080/contacts. Below are the supported methods:

|Method|Path|Usage|
|---|---|---|
|GET|/contacts|Get all contacts currently stored in the database|
|POST|/contacts|Create a new contact entry in the database|
|GET|/contacts/{id}|Get contact with said ID from the database|
|PUT|/contacts/{id}|Update contact with said ID in the database|
|DELETE|/contacts/{id}|Delete contact with said ID in the database|