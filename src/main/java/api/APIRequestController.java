package api;

import entities.Contact;
import hibernate.HibernateManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/contacts")
public class APIRequestController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getContacts() {
		Response response = null;
		HibernateManager mgr = new HibernateManager();
		try {
			List<Contact> contacts= mgr.getAllContacts();
			response = Response.status(Response.Status.OK).entity(contacts).build();
		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addContact(Contact newContact) {
		Response response = null;
		HibernateManager mgr = new HibernateManager();
		try {
			int id = mgr.addContact(newContact);
			response = Response.status(Response.Status.OK).entity("Contact successfully added: " + id).build();
		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;
	}

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getContact(@PathParam("id") Integer id) {
		Response response = null;
		HibernateManager mgr = new HibernateManager();
		try {
			Contact contact= mgr.getContact(id);
			response = Response.status(Response.Status.OK).entity(contact).build();
		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;
	}

	@Path("/{id}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateContact(@PathParam("id") Integer id, Contact contactData) {
		Response response = null;
		HibernateManager mgr = new HibernateManager();
		try {
			mgr.updateContact(id, contactData);
			response = Response.status(Response.Status.OK).entity("Contact successfully updated: " + id).build();
		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;
	}

	@Path("/{id}")
	@DELETE
	public Response deleteContact(@PathParam("id") Integer id) {
		Response response = null;
		HibernateManager mgr = new HibernateManager();
		try {
			mgr.deleteContact(id);
			response = Response.status(Response.Status.OK).entity("Contact successfully deleted: " + id).build();
		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;
	}

}
