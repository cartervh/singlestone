package entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="contacts")
public class Contact implements Serializable {

	@Id @GeneratedValue
	@Column(name="ID")
	private int id;

	@Column(name="email")
	private String email;

	@OneToOne(cascade= CascadeType.ALL)
	@JoinColumn(name="addressID")
	private Address address = new Address();

	@OneToOne(cascade= CascadeType.ALL)
	@JoinColumn(name="nameID")
	private Name name = new Name();

	@OneToMany(cascade= CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "contactID")
	private Set<Phone> phone = new HashSet<Phone>();

	public int getID() {
		return id;
	}

	public Address getAddress() {
		return address;
	}

	public Name getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Phone> getPhone() {
		return phone;
	}

	public void setPhone(Set<Phone> phone) {
		this.phone.clear();
		this.phone.addAll(phone);
	}

	public void addPhone(Phone phone) {
		this.phone.add(phone);
	}

	public void removePhone(Phone phone) {
		this.phone.remove(phone);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o instanceof Contact) {
			Contact c = (Contact) o;
			boolean isEqual = true;
			isEqual = isEqual && compare(this.getName(), c.getName());
			isEqual = isEqual && compare(this.getAddress(), c.getAddress());
			isEqual = isEqual && compare(this.getEmail(), c.getEmail());
			boolean phonesEqual = true;
			if (this.getPhone() != null || c.getPhone() != null) {
				if (this.getPhone() != null && c.getPhone() == null) {
					phonesEqual = this.getPhone().size() == 0;
				} else if (c.getPhone() != null && this.getPhone() == null) {
					phonesEqual = c.getPhone().size() == 0;
				} else {
					phonesEqual = c.getPhone().equals(this.getPhone());
				}
			}
			isEqual = isEqual && phonesEqual;
			return isEqual;
		} else {
			return false;
		}

	}

	//This is a one off method, kept here to avoid making a whole Util class.
	public static boolean compare(Object obj1, Object obj2) {
		return (obj1 == null ? obj2 == null : obj1.equals(obj2));
	}
}
