package entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="phones")
public class Phone implements Serializable {

	public Phone() {
		//Do nothing
	}

	public Phone(PhoneType type, String number) {
		this.number = number;
		this.type = type;
	}

	public enum PhoneType {
		home {
			public String toString() {
				return "home";
			}
		},
		work {
			@Override
			public String toString() {
				return "work";
			}
		},
		mobile {
			@Override
			public String toString() {
				return "mobile";
			}
		};
	}

	@Id @GeneratedValue
	@Column(name="ID")
	@JsonIgnore
	private int phoneID;

	@Column(name="number")
	private String number;

	@Enumerated(EnumType.STRING)
	@Column(name="type", nullable=false)
	private PhoneType type;

	public int getPhoneID() {
		return phoneID;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public PhoneType getType() {
		return type;
	}

	public void setType(PhoneType type) {
		this.type = type;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Phone phone = (Phone) o;
		return phoneID == phone.phoneID &&
				Objects.equals(number, phone.number) &&
				type == phone.type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(phoneID, number, type);
	}
}