package hibernate;

import entities.Address;
import entities.Contact;
import entities.Name;
import entities.Phone;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.service.ServiceRegistry;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Iterator;
import java.util.List;

public class HibernateManager {

	private static SessionFactory sessionFactory = buildSessionFactory();

	public int addContact(Contact newContact) {
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			session.save(newContact);
			session.getTransaction().commit();
			return newContact.getID();
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public Contact getContact(int id) {
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			Contact contact = session.get(Contact.class, id);
			session.getTransaction().commit();
			return contact;
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public List<Contact> getAllContacts() {
			Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Contact> criteria = builder.createQuery(Contact.class);
			criteria.from(Contact.class);
			List<Contact> contacts = session.createQuery(criteria).getResultList();
			session.getTransaction().commit();
			return contacts;
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public void updateContact(int id, Contact newValues) {
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			Contact contact = session.get(Contact.class, id);

			contact.getName().setFirst(newValues.getName().getFirst());
			contact.getName().setMiddle(newValues.getName().getMiddle());
			contact.getName().setLast(newValues.getName().getLast());
			contact.setEmail(newValues.getEmail());

			contact.getAddress().setStreet(newValues.getAddress().getStreet());
			contact.getAddress().setCity(newValues.getAddress().getCity());
			contact.getAddress().setState(newValues.getAddress().getState());
			contact.getAddress().setZip(newValues.getAddress().getZip());

			contact.setPhone(newValues.getPhone());

			session.update(contact);
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public void deleteContact(int id) {
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			Contact contact = session.get(Contact.class, id);
			session.delete(contact);
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public Long getContactRowCount() {

		Session session = sessionFactory.openSession();
		try {
			Long retVal = 0l;
			session.beginTransaction();

			String queryStr = "SELECT COUNT(*) FROM Contact contacts";
			Query query = session.createQuery(queryStr);
			Iterator iter = query.iterate();
			while (iter.hasNext()) {
				retVal = (Long) iter.next();
			}
			session.getTransaction().commit();

			return retVal;
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public Long getPhoneRowCount() {
		Session session = sessionFactory.openSession();
		try {
			Long retVal = 0l;
			session.beginTransaction();

			String queryString = "SELECT COUNT(*) FROM Phone phones";
			Query query = session.createQuery(queryString);
			Iterator iter = query.iterate();
			while (iter.hasNext()) {
				retVal = (Long) iter.next();
			}
			session.getTransaction().commit();
			return retVal;
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	private static SessionFactory buildSessionFactory()
	{
		try
		{
			if (sessionFactory == null)
			{
				Configuration cfg = new Configuration().configure();
				cfg.setProperty("connection.url", "jdbc:h2:mem:apichallenge;DB_CLOSE_DELAY=-1;INIT=runscript from 'classpath:create-db.sql';");
				cfg.setProperty("connection.driver_class", "org.h2.Driver");
				cfg.setProperty("hibernate.connection.username", "sa");
				cfg.setProperty("hibernate.connection.password", "");
				cfg.setProperty("dialect", "org.hibernate.dialect.H2Dialect");
				cfg.setProperty("show_sql", "true");
				cfg.setProperty("hbm2ddl.auto", "create");
				cfg.addAnnotatedClass(Contact.class).addAnnotatedClass(Phone.class).addAnnotatedClass(Name.class).addAnnotatedClass(Address.class);
				StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
				serviceRegistryBuilder.applySettings(cfg.getProperties());
				ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
				sessionFactory = cfg.buildSessionFactory(serviceRegistry);
			}
			return sessionFactory;
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
}
