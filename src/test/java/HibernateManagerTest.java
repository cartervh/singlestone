import entities.Contact;
import entities.Phone;
import entities.Phone.PhoneType;
import hibernate.HibernateManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HibernateManagerTest {

    private static HibernateManager mgr = null;

    @BeforeEach
    private void setUpBeforeRun() {
        if (mgr == null) {
            mgr = new HibernateManager();
        }
    }

    /*
     * I don't see a great way to create a test that can individually test add without get and vice-versa, so they
     * are being tested together here.
     */
    @Test
    void testAddGetContact() {
        //First contact create
        Contact c1 = new Contact();
        c1.getName().setFirst("Carter");
        c1.getName().setMiddle("A");
        c1.getName().setLast("VanHuss");
        c1.getAddress().setStreet("123 Test St.");
        int id = mgr.addContact(c1);

        Contact savedContact1 = mgr.getContact(id);
         assertEquals(c1, savedContact1);

        //Second contact create
        Contact c2 = new Contact();
        c2.getName().setFirst("Johnny");
        c2.getName().setLast("Bravo");

        Phone p1 = new Phone(PhoneType.home, "1-800-867-5309");

        Phone p2 = new Phone(PhoneType.mobile, "1-234-567-8910");

        c2.addPhone(p1);
        c2.addPhone(p2);

        id = mgr.addContact(c2);

        Contact savedContact2 = mgr.getContact(id);

        assertEquals(c2, savedContact2);
    }

    @Test
    void getAllContacts() {
        //Setup
        Long numContactsBefore = mgr.getContactRowCount();
        Long numPhonesBefore = mgr.getPhoneRowCount();

        Contact c1 = new Contact();
        c1.getName().setFirst("Scott");
        c1.getName().setLast("Kennedy");
        c1.getAddress().setStreet("33 Burbank Street");
        c1.setEmail("skennedy@test.com");
        mgr.addContact(c1);

        Contact c2 = new Contact();
        c2.getName().setFirst("Amy");
        c2.getName().setLast("Brandon");
        Phone p1 = new Phone(PhoneType.work, "1-098-765-4321");
        Phone p2 = new Phone(PhoneType.mobile, "1-111-111-1111");
        c2.addPhone(p1);
        c2.addPhone(p2);
        mgr.addContact(c2);

        Contact c3 = new Contact();
        c3.getName().setFirst("Steven");
        c3.getName().setLast("Tyler");
        Phone p3 = new Phone(PhoneType.home, "2-222-222-2222");
        c3.addPhone(p3);
        mgr.addContact(c3);

        //Test
        List<Contact> contactList = mgr.getAllContacts();
        assertEquals(numContactsBefore + 3, contactList.size());
        assertEquals(numPhonesBefore + 3, mgr.getPhoneRowCount());
        assertTrue(contactList.contains(c1));
        assertTrue(contactList.contains(c2));
        assertTrue(contactList.contains(c3));
    }

    @Test
    void updateContactAddPhone() {
        //Contact create (test adding phones)
        Contact c1 = new Contact();
        c1.getName().setFirst("Jeff");
        c1.getName().setMiddle("A");
        c1.getName().setLast("Bezos");
        int id = mgr.addContact(c1);

        //Update first contact
        Contact update1 = new Contact();
        update1.getName().setFirst("Carlos");
        update1.getName().setLast("Santana");
        update1.getAddress().setStreet("11 Amazon Avenue");
        Phone p1 = new Phone(PhoneType.home, "1-800-867-5309");
        update1.addPhone(p1);
        mgr.updateContact(id, update1);

        Contact foundContact = mgr.getContact(id);
        assertEquals(update1, foundContact);
    }

    @Test
    void updateContactRemovePhone() {

    //Second contact create (test removing phones)
    Contact c2 = new Contact();
    c2.getName().setFirst("Steve");
    c2.getName().setLast("Jobs");
    c2.getAddress().setStreet("33 Apple Ct");
    Phone p2 = new Phone(PhoneType.mobile, "3-333-333-3333");
    c2.addPhone(p2);
    int id = mgr.addContact(c2);

    Contact update2 = new Contact();
    update2.getName().setFirst("Jony");
    update2.getName().setLast("Ives");

    Long countPhonesBefore = mgr.getPhoneRowCount();
    mgr.updateContact(id, update2);
    assertEquals(update2, mgr.getContact(id));
    assertEquals(countPhonesBefore - 1, mgr.getPhoneRowCount());
    }

    @Test
    void deleteContact() {
        //Add new contact
        Contact c1 = new Contact();
        c1.getName().setFirst("Alfred");
        c1.getName().setLast("Hitchcock");
        c1.getAddress().setStreet("44 Delete Street");
        int id = mgr.addContact(c1);

        //Store size of DB before delete
        Long sizeBeforeDelete = mgr.getContactRowCount();

        mgr.deleteContact(id);

        assertEquals(sizeBeforeDelete - 1, mgr.getContactRowCount());

        Contact deletedContact = mgr.getContact(id);
        assertNull(deletedContact);
    }
}